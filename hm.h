#ifndef __HM_H__
#define __HM_H__

#define INITIAL_HEAP_SIZE 1000
#define MMAP_THRESHOLD 10024

struct bloc{
  size_t size_to_malloc;
  int state;
  struct bloc *next;
  struct bloc *predecessor;
};


void init_heap();
void *malloc(size_t size);
void free(void *ptr);
void *realloc(void *ptr, size_t size);
void itere_heap();

#endif
