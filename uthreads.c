#include "uthreads.h"

#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>

#define FILEPATH "/tmp/shared_mem.bin"
#define FILESIZE (1 << 22)

uthread_t uthreads[MAX_UTHREADS]; // Le tableau contenant tous les uthreads
vcpu_t vcpus[MAX_VCPUS]; // Le tableau contenant tous les vCPUs

unsigned int last_scheduled = 0; // L'indice du deriner uthread à avoir été schedulé
unsigned int number_of_created_uthreads = 0; // Le nombre de uthreads créés
unsigned int number_of_active_uthreads = 0;
unsigned int number_of_vcpus = 0; // Le nombre de vCPUs
unsigned int next_number_of_vcpus = 0; // Le nombre de vCPUs à garder au prochain scheduling
unsigned int quanta = 500; // en millisecondes
int vm_id; // Identifiant de la VM pour le load balancing (0 ou 1)
pthread_mutex_t scheduling; // Les verrou de l'ordonnancement
uthread_t uthread_idle; // N'est utilisé que pour initialiser les vCPUs idles
int fd; //le descripteur du fichier partagé
int *map; //le mapping de ce dit fichier
unsigned int doit_exporter =0; //flag soulevé par send_sigs si le load_balancer a demandé un envoi, alors `sched` va transmettre le premier qu'il peut

//est lancé une fois que le contexte est redevenu actif
//il se fait alors envoyer ses signaux en attente
void receptionne_signaux(uthread_t thread)
{
  _thread_file *chainon = &thread.signaux;
  while (chainon->successeur != NULL && thread.is_active)
    {
      pthread_kill(vcpus[thread.vcpu].thread, chainon->value);
      chainon = chainon->successeur;
      thread.signaux = *chainon;
    }
}

//appelé à la sortie par un `atexit`, s'assure que le fichier partagé soit bien fermé
//et que l'état du processus soit marqué comme "eteint"
void fermeture_du_fichier()
{
  printf("on va fermer le fichier\n");
  map[vm_id*3] = 0; //on indique au load_balancer qu'on a terminé
  if (msync((void *) map, FILESIZE, MS_SYNC) == -1){
    perror("error in synchronizing the file before unmapping it");
  }
  
  if (munmap(map, FILESIZE) == -1) {
    perror("Error un-mmapping the file");
  }
  
  printf("fermeture standard du processus\n");
  close(fd);
}

//fonction appelée pour transférer un uthread vers l'autre processus : on va copier tous les éléments qui ne sont pas propre à ce processus (donc essentiellement le contenu du contexte) vers le fichier partagé.
//On marque aussi ce thread comme étant "mort" et "à libérer en mémoire" dans ce processus
//finalement on marque dans le fichier partagé que la copie a bien été effectuée
void uthread_export(void* addr, uthread_t* thread)
{
  void* p = addr;
  if (addr == NULL)
    return;

  pthread_mutex_lock(&scheduling);
  number_of_active_uthreads--;
  thread->is_dead=1;
  thread->to_be_freed = 1;
  // Copie du thread
  *((sigset_t*) p) = thread->ctx.uc_sigmask;
  p += sizeof(sigset_t);
  *((size_t*) p) = thread->ctx.uc_stack.ss_size;
  p += sizeof(size_t);
  memcpy(p, thread->ctx.uc_stack.ss_sp, thread->ctx.uc_stack.ss_size);
  p += thread->ctx.uc_stack.ss_size;
  *((ucontext_t*) p) = thread->ctx;  
  pthread_mutex_unlock(&scheduling);
  //puis on marque que la copie a bien été effectuée
  map[vm_id+6] = 2;
  printf("\e[1;33muthread donné\e[0m\n");
}

//on va aller copier dans notre tableau des uthreads le contexte dans le dossier en mémoire partagé
//Puis on lui initialise toutes les valeurs de `uthread` standard, tel dans `uthread_create`
void uthread_import(void* addr)
{
  void* p = addr;
  if (addr == NULL)
    return;

  uthread_t *thread;
  int id;
  pthread_mutex_lock(&scheduling);

  //obtention d'une case pour le uthread
  unsigned int i, found;
  for (i = found = 0; i<number_of_created_uthreads && !found; i++)
    {
      if(uthreads[i].is_dead)
	{
	  if (uthreads[i].to_be_freed)
	    {
	      free(uthreads[i].ctx.uc_stack.ss_sp);
	      uthreads[last_scheduled].to_be_freed = 0;
	    }
	  id = i;
	  found = 1;
	}
    }
  if (found)
    thread = &uthreads[id];
  else
    thread = &uthreads[(id = number_of_created_uthreads++)];

  number_of_active_uthreads++;  
  // Copie du thread
  thread->ctx.uc_sigmask = *((sigset_t*) p);
  p += sizeof(sigset_t);
  thread->ctx.uc_stack.ss_size =*((size_t*) p);
  p += sizeof(size_t);
  thread->ctx.uc_stack.ss_sp = malloc(thread->ctx.uc_stack.ss_size);
  memcpy(thread->ctx.uc_stack.ss_sp, p, thread->ctx.uc_stack.ss_size);
  p += thread->ctx.uc_stack.ss_size;
  thread->ctx =*((ucontext_t*) p);  
  pthread_mutex_unlock(&scheduling);
  thread->ctx.uc_link = &uthread_idle.ctx;

  thread->is_dead = 0;
  thread->is_active = 0;
  thread->id = id;
  thread->is_joined = 0;
  _thread_file a;
  a.successeur = NULL;
  thread->signaux = a;
  thread->fin_file = &(thread->signaux);
  thread->result = NULL;
  thread->to_be_freed = 0;
  printf("\e[1;33muthread lu et recopié\e[0m\n");
}

// Le coeur du scheduler
// À chaque quanta, une instance du scheduler est appelée sur chaque vCPU
// C'est le handler de SIGUSR1 (envoyé par send_sigs)
// Il fait le changement de contexte et la mise à jour de la file des processus
// S'il n'y a rien à scheduler, il se met en idle
void sched(int signo)
{
  (void) signo;
  pthread_mutex_lock(&scheduling);
  unsigned int old_process = last_scheduled;
  pthread_t tid = pthread_self();
  unsigned int i;
  ucontext_t *tmp, *tmp2;
  if (number_of_created_uthreads == 0)
  {
    pthread_mutex_unlock(&scheduling);
    return;
  }
  // On cherche quel vCPU on est
  for (i=0; vcpus[i].thread != tid; i++);
  vcpus[i].active_process->is_active = 0;
  if (vcpus[i].status == VCPU_DYING || vcpus[i].status == VCPU_DEAD)
    {
      printf("VCPU %d : sched : %d -> sleep\n", i, vcpus[i].active_process->id);
      tmp = &vcpus[i].active_process->ctx;
      vcpus[i].active_process = &vcpus[i].idle;
      vcpus[i].status = VCPU_DEAD;
      pthread_mutex_unlock(&scheduling);
      if (tmp != &vcpus[i].idle.ctx)
	swapcontext(tmp, &vcpus[i].idle.ctx);
      return;
    }
  do
  {
    last_scheduled = (last_scheduled + 1) % number_of_created_uthreads;
    if (uthreads[last_scheduled].to_be_freed && uthreads[last_scheduled].id != vcpus[i].active_process->id)
    {
      printf("\e[1;30;42mLibération de mémoire !\e[0m\n");
      free(uthreads[last_scheduled].ctx.uc_stack.ss_sp);
      uthreads[last_scheduled].to_be_freed = 0;
    }
  }
  while ((uthreads[last_scheduled].is_dead || uthreads[last_scheduled].is_active) && last_scheduled != old_process);
  // On regarde si on est en train de schedule le seul uthread actif
  if (last_scheduled != old_process || (vcpus[i].active_process->id >= 0 && !vcpus[i].active_process->is_dead))
  {
    printf("VCPU %d : sched : %d -> %d\t\n", i, vcpus[i].active_process->id, last_scheduled);
    uthreads[last_scheduled].is_active = 1;
    uthread_t* old_uthread = vcpus[i].active_process;
    tmp = &vcpus[i].active_process->ctx;
    tmp2 = &uthreads[last_scheduled].ctx;
    vcpus[i].active_process = &uthreads[last_scheduled];
    pthread_mutex_unlock(&scheduling);
    swapcontext(tmp, tmp2);
    //puis, si on doit exporter, on vérifie que le thread libéré peut l'être, et le cas échéant on l'exporte
    if (doit_exporter && !old_uthread->is_joined /*&& old_uthread->signaux.successeur == NULL*/)
      {
	doit_exporter = 0;
	uthread_export(map+8, old_uthread);
      }
    
    //maintenant que le uthread est à nouveau actif, on lui fait rattraper ses signaux
    receptionne_signaux(uthreads[last_scheduled]);
  }
  else
  {
    printf("VCPU %d : sched : %d -> idle\n", i, vcpus[i].active_process->id);
    tmp = &vcpus[i].active_process->ctx;
    vcpus[i].active_process = &vcpus[i].idle;
    pthread_mutex_unlock(&scheduling);
    if (tmp != &vcpus[i].idle.ctx)
      swapcontext(tmp, &vcpus[i].idle.ctx);
  }
}


// Le handler de SIGALRM
// On met alors à jour les informations données au load_balancer, puis on regarde si le load_balancer a mis à jour une information nous concernant
// Appelle le scheduler sur chaque vCPU
void send_sigs(int signo)
{
  (void) signo;
  printf("\e[1;35msend_sigs\e[0m\n");
  unsigned int i = 3*vm_id;
  map[i+1] = number_of_vcpus;
  map[i+2] = number_of_active_uthreads; //à modifier pour que ça devienne les uthread actifs
  if (map[6+vm_id]==1)
    {//1 signifie qu'on nous demande un uthread
      doit_exporter = 1; //marque que l'on veut transférer un uthread
      printf("\e[1;33muthread bientôt donné\e[0m\n");
    }
  else if (map[6+vm_id] == 3)
    {//3 signifie qu'on a reçu un uthread
      uthread_import(map+8);
      map[6+vm_id] = 0;
      printf("\e[1;33muthread reçu\e[0m\n");
    }
  for (i=0; i<number_of_vcpus; i++)
  {
    pthread_kill(vcpus[i].thread, SIGUSR1);
  }
  if (next_number_of_vcpus != 0)
    {
      number_of_vcpus = next_number_of_vcpus;
      next_number_of_vcpus = 0;
    }
}

// Un vcpu qui ne fait rien
// Il attend passivement un signal, en boucle
void* vcpu_idle(void* args)
{
  (void) args;
  pthread_t self = pthread_self();
  unsigned int i;
  for (i=0; vcpus[i].thread != self; i++);
  getcontext(&vcpus[i].idle.ctx);
  while (1)
  {
    printf("\e[1;33midle %d dit coucou !\e[0m\n", i);
    pause();
  }
  return NULL;
}

// Un handler vide pour SIGUSR2
// Il permet de ne pas tuer le processus à l'arrivée d'un SIGUSR2, qui est
// le signal envoyé à un processus qui attend la terminaison d'un uthread avec
// uthread_join
void sigusr2_handler(int signo)
{
  (void) signo;
}  

// Initialise les `number` vCPUs
// Initialise les handlers des signaux pour gérer les vCPUs
// Initialise le timer qui appelle le scheduler tous les quanta
// Les vCPUs sont initialement idles
// Renvoie -1 en cas d'erreur, 0 sinon
// La fonction ne peut être appelée qu'une fois
int init_vcpus(unsigned int number, int id)
{  
  if (number_of_vcpus > 0 || number > MAX_VCPUS || number == 0
      || (id != 0 && id != 1))
    return -1;
  // else
  unsigned int i;
  struct sigaction sa_alarm, sa_sched, sa_usr2;

  number_of_vcpus = number;
  vm_id = id;

  for (i=0; i<number; i++)
  {
    getcontext(&vcpus[i].idle.ctx);
    vcpus[i].status = VCPU_ALIVE;
    vcpus[i].active_process = &vcpus[i].idle;
    vcpus[i].idle.is_dead = 0;
    vcpus[i].idle.is_active = 1;
    vcpus[i].idle.id = -(i+1);
    vcpus[i].idle.ctx.uc_stack.ss_sp = malloc(STACK_SIZE);
    vcpus[i].idle.ctx.uc_stack.ss_size = STACK_SIZE;
    vcpus[i].idle.ctx.uc_link = NULL;
    makecontext(&vcpus[i].idle.ctx, (void*) vcpu_idle, 0);
    pthread_create(&vcpus[i].thread, NULL, vcpu_idle, NULL);
  }
  printf("début de l'ouverture\n");
  //puis on ouvre le fichier partagé et on y met qu'on a bien démarré
  fd = open(FILEPATH, O_RDWR | O_CREAT | O_SYNC, (mode_t)0700);
  if (fd == -1) {
    perror("Error opening file for writing");
    exit(EXIT_FAILURE);
  }
  
  if (lseek(fd, FILESIZE-1, SEEK_SET) == -1) {
    close(fd);
    perror("Error calling lseek() to 'stretch' the file");
    exit(EXIT_FAILURE);
  }
  //puis a priori il faut écrire un bit à la fin du fichier pour s'assurer que le fait de l'avoir étendu ait marché
  if (write(fd, "", 1) != 1) {
    close(fd);
    perror("Error writing last byte of the file");
    exit(EXIT_FAILURE);
  }
  printf("fichier ouvert et étendu\n");
    
  map = mmap(0, FILESIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (map == MAP_FAILED) {
    close(fd);
    perror("Error mmapping the file");
    exit(EXIT_FAILURE);
  }
    
  //i représente le début des deux int réservés pour ce processus, où il pourra écrire son état
  i = 3*vm_id;
  printf("on a écrit en case %i\n", i);
  map[i] = 100; //100 = j'ai bien démarré
  if (atexit (fermeture_du_fichier) != 0)
    {
      perror("cannot set the exit function : shutting process");
      close(fd);
      exit(EXIT_FAILURE);
      }
  printf("ouverture finie du fichier partagé\n");

  
  //on ne met en place l'alarme qu'après avoir mis le bon 'numero', afin qu'il n'aille pas écrire n'importe où
  // Initialisation du quanta
  set_quanta(quanta);
  // Initialisation des signaux
  sa_alarm.sa_handler = send_sigs; // SIGALRM
  sa_alarm.sa_flags = 0;
  sigemptyset(&sa_alarm.sa_mask);
  sigaddset(&sa_alarm.sa_mask, SIGALRM);
  sigaddset(&sa_alarm.sa_mask, SIGALRM);
  sigaction(SIGALRM, &sa_alarm, NULL);
  sa_sched.sa_handler = sched; // SIGUSR1
  sa_sched.sa_flags = 0;
  sigemptyset(&sa_sched.sa_mask);
  sigaddset(&sa_sched.sa_mask, SIGUSR1);
  sigaddset(&sa_sched.sa_mask, SIGALRM);
  sigaction(SIGUSR1, &sa_sched, NULL);
  sa_usr2.sa_handler = sigusr2_handler; // SIGUSR2
  sa_usr2.sa_flags = 0;
  sigemptyset(&sa_sched.sa_mask);
  sigaction(SIGUSR2, &sa_usr2, NULL);
  // création du verrou pour le scheduling
  pthread_mutex_init(&scheduling, NULL);
  
  return 0;
}

// Modifie le nombre de VCPUs
void set_vcpus(unsigned int number)
{
  if (number == 0 || number == number_of_vcpus || number > MAX_VCPUS)
    return;

  unsigned int i;
  pthread_mutex_lock(&scheduling);
  if (number > number_of_vcpus) // Ajout de VCPUs
  {
    for (i=number_of_vcpus; i<number; i++)
      {
	if (vcpus[i].status == VCPU_NOT_INITIALIZED)
	  {
	    getcontext(&vcpus[i].idle.ctx);
	    vcpus[i].status = VCPU_ALIVE;
	    vcpus[i].active_process = &vcpus[i].idle;
	    vcpus[i].idle.is_dead = 0;
	    vcpus[i].idle.is_active = 1;
	    vcpus[i].idle.id = -(i+1);
	    vcpus[i].idle.ctx.uc_stack.ss_sp = malloc(STACK_SIZE);
	    vcpus[i].idle.ctx.uc_stack.ss_size = STACK_SIZE;
	    vcpus[i].idle.ctx.uc_link = NULL;
	    makecontext(&vcpus[i].idle.ctx, (void*) vcpu_idle, 0);
	    pthread_create(&vcpus[i].thread, NULL, vcpu_idle, NULL);
	  }
	else
	  {
	    vcpus[i].status = VCPU_ALIVE;
	    vcpus[i].active_process = &vcpus[i].idle;
	  }
	number_of_vcpus = number;
      }
  }
  else // Retrait de VCPUs
  {
    next_number_of_vcpus = number;
    for (i=number; i<number_of_vcpus; i++)
      vcpus[i].status = VCPU_DYING;
  }
  pthread_mutex_unlock(&scheduling);
}

// Crée un uthread
// Remplit la structure `*thread` passée en argument
// Démarre le uthread sur la fonction `start_routine` avec `arg` en argument
// Renvoie -1 en cas d'erreur, 0 sinon
int uthread_create(uthread_t *thread, void* (*start_routine) (void *), void *arg)
{
  int id;
  uthread_t* ucp; // = &uthreads[(id = number_of_created_uthreads++)]; // le nouveau processus
  void (*func) () = (void*) start_routine; // simplement pour éviter les warnings
  char* stack = NULL;
  unsigned int i;
  int found;
  // initialisation du contexte
  if ((stack = malloc(STACK_SIZE)) == NULL)
  {
    number_of_created_uthreads--;
    return -1; // on n'a pas pu allouer la pile, on ne peut pas continuer
  }
  // On cherche le plus petit id de uthread inutilisé
  pthread_mutex_lock(&scheduling);
  for (i = found = 0; i<number_of_created_uthreads && !found; i++)
    {
      if(uthreads[i].is_dead)
	{
	  if (uthreads[i].to_be_freed)
	    {
	      free(uthreads[i].ctx.uc_stack.ss_sp);
	      uthreads[last_scheduled].to_be_freed = 0;
	    }
	  id = i;
	  found = 1;
	}
    }
  number_of_active_uthreads++;
  if (found)
    ucp = &uthreads[id];
  else
    ucp = &uthreads[(id = number_of_created_uthreads++)];
  getcontext(&ucp->ctx);
  ucp->ctx.uc_stack.ss_sp = stack;
  ucp->ctx.uc_stack.ss_size = STACK_SIZE;
  ucp->ctx.uc_link = &uthread_idle.ctx; // On ne veut pas s'en servir
  ucp->is_dead = 0;
  ucp->is_active = 0;
  ucp->id = id;
  ucp->is_joined = 0;
  _thread_file a;
  a.successeur = NULL;
  ucp->signaux = a;
  ucp->fin_file = &(ucp->signaux);
  ucp->result = NULL;
  ucp->to_be_freed = 0;
  makecontext(&ucp->ctx, func, 1, arg);
  if (thread != NULL)
    *thread = *ucp;
  pthread_mutex_unlock(&scheduling);
  return 0;
}

// modifie la fréquence des appels au scheduler
// La fréquence passe à `new_quanta` microsecondes
void set_quanta(unsigned int new_quanta)
{
  struct itimerval it_val;
  quanta = new_quanta;
  it_val.it_value.tv_sec = new_quanta/1000;
  it_val.it_value.tv_usec = (new_quanta*1000) % 1000000;
  it_val.it_interval = it_val.it_value;
  setitimer(ITIMER_REAL, &it_val, NULL);
}

// Appelé par un uthread, rend la main au scheduler
void uthread_yield(void)
{
  printf("\e[1;34mYIELD !\e[0m\n");
  pthread_kill(pthread_self(), SIGUSR1);
}

// Appelé par un uthread, finit l'exécution du uthread
// `value` sera renvoyé au processus qui avait `join` cec uthread
void uthread_exit(void *value)
{
  unsigned int i;
  pthread_t self;

  pthread_mutex_lock(&scheduling);
  self = pthread_self();
  for (i=0; i<number_of_created_uthreads && vcpus[i].thread != self; i++);
  vcpus[i].active_process->result = value;
  vcpus[i].active_process->is_dead = 1;
  vcpus[i].active_process->to_be_freed = 1;
  number_of_active_uthreads--;
  if (vcpus[i].active_process->is_joined)
    pthread_kill(vcpus[i].active_process->destinataire, SIGUSR2);
  pthread_mutex_unlock(&scheduling);
  uthread_yield();
}


// Attend la terminaison du uthread `thread`
// S'il était déjà fini, ou s'il vient de se finir, alors sa valeur de retour
// est stockée dans `*retval`
void uthread_join(uthread_t thread, void **retval)
{
  printf("\e[1;35mDébut du join sur %d\e[0m\n", thread.id);
  unsigned int i;
  pthread_mutex_lock(&scheduling);
  for (i=0; i<number_of_created_uthreads && thread.id != uthreads[i].id; i++);
  uthreads[i].destinataire = pthread_self();
  uthreads[i].is_joined = 1;
  pthread_mutex_unlock(&scheduling);
  int sig;
  sigset_t set;
  sigemptyset(&set);
  sigaddset(&set, SIGUSR2);
  if (!uthreads[i].is_dead)
    {
      pthread_mutex_unlock(&scheduling);
      printf("\e[1;44mJoin en attente de %d\e[0m\n", thread.id);
      sigwait(&set, &sig);
    }
  else
  {
    pthread_mutex_unlock(&scheduling);
  }
  if (retval != NULL)
    *retval = uthreads[i].result;
  printf("\e[1;35mFin du join sur %d\e[0m\n", thread.id);
}

// envoie un signal à un uthread
// soit immédiatement, soit le met en attente dans la file du uthread
void uthread_kill(uthread_t thread, int sig)
{
  printf("\e[1;31menvoie un signal au thread %d\e[0m\n", thread.id);
  pthread_mutex_lock(&scheduling);
  unsigned int i;
  for (i=0; i<number_of_created_uthreads && uthreads[i].id != thread.id; i++);
  if (thread.is_active) 
    pthread_kill(vcpus[thread.vcpu].thread, sig);
  else
    {
      printf("\e[1;31mle signal sera traité plus tard\e[0m\n");
      _thread_file *signal=malloc(sizeof (_thread_file)); 
      signal->successeur = NULL;
      signal->value = sig;
      (uthreads[i].fin_file)->successeur = signal;
      uthreads[i].fin_file = signal;
    }
  pthread_mutex_unlock(&scheduling);
}
