#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <signal.h>

#define FILEPATH "/tmp/shared_mem.bin"
#define FILESIZE (1<<22)

int *map;  /* mmapped array of int's */
unsigned int quanta = 500; //le quanta, période entre deux appels de la fonction d'équilibrage
int fd;

void set_quanta(unsigned int new_quanta)
{
  struct itimerval it_val;
  quanta = new_quanta;
  it_val.it_value.tv_sec = new_quanta/1000;
  it_val.it_value.tv_usec = (new_quanta*1000) % 1000000;
  it_val.it_interval = it_val.it_value;
  setitimer(ITIMER_REAL, &it_val, NULL);
}

//appelé à la sortie par un `atexit`, s'assure que le fichier partagé soit bien fermé
//et que les messages envoyés soient interrompus : on veut que toute activité de transfert cesse
void fermeture_du_fichier()
{
  printf("on va fermer le fichier\n");
  //on réinitialise les signaux envoyés, laissant donc les processus en plan
  map[6] = 0;
  map[7] = 0;
  if (msync((void *) map, FILESIZE, MS_SYNC) == -1){
    perror("error in synchronizing the file before unmapping it");
  }
  
  if (munmap(map, FILESIZE) == -1) {
    perror("Error un-mmapping the file");
  }
  
  printf("fermeture standard du processus\n");
  close(fd);
}

//on vérifie que les deux processus sont bien actifs, puis on regarde si l'envoi a été traité
// si on en avait demandé un (on a alors un `2` dans la case correspondante). On dit alors au processus qu'on a vu qu'il avait accompli sa mission, puis on informe l'autre qu'il a un contexte à récupérer
//une fois ceci fait, si on avait pas de transfert en cours, on vérifie si un nouveau transfert n'a pas lieu d'être : si il y un déséquilibre de charge par vcpu trop important et que cette charge est strictement supérieure à 1
void equilibre()
{
  if (map[0] == 100 && map[3] == 100)
    {
      printf("les deux ont bien démarré\n");
      if (map[6] == 2)
	{
	  map[6]=0; //on confirme que l'emission a été vue
	  map[7]=3;
	  printf("on confirme que 0 a ecrit\n");
	}
      else if (map[7] == 2)
	{
	  map[7]=0; //on confirme que l'emission a été vue
	  map[6]=3;
	  printf("on confirme que 1 a ecrit\n");
	}
      else if (map[6]==0 && map[7]==0)
	{
	  int charge0 = map[2]*map[4]; //donc le nombre de uthreads du 1er fois le nombre de vcpu du 2nd
	  int charge1 = map[5]*map[1]; //l'inverse
	  printf("on compare %i et %i\n", charge0, charge1);
	  if (charge0>charge1 && (charge0-map[4])>=(charge1+map[1])&&(map[2]-1)>map[1])
	    {
	      map[6]=1; //on requiert le transfert d'un uthread vers le second, qui restera moins chargé après l'opération (on évite les transferts charchant un équilibre impossible)
	      printf("c'est a 0 de transferer\n");
	    }
	  else if (charge1>charge0 && (charge1-map[1])>=(charge0+map[4])&&(map[5]-1)>map[4])
	    {
	      map[7]=1; //on requiert le transfert d'un uthread
	      printf("c'est a 1 de transferer\n");
	    }
	}
    }
  else
    {
      map[6]=0;
      map[7]=0;
    }
}

//on ouvre le fichier partagé puis déclenche l'alarme qui appelera `equilibre`
int main()
{
  int fd;
  int result;
  
  fd = open(FILEPATH, O_RDWR | O_CREAT | O_SYNC, (mode_t)0700);
  if (fd == -1) {
    perror("Error opening file for writing");
    exit(EXIT_FAILURE);
  }
  
  result = lseek(fd, FILESIZE-1, SEEK_SET);
  if (result == -1) {
    close(fd);
    perror("Error calling lseek() to 'stretch' the file");
    exit(EXIT_FAILURE);
  }
  
  result = write(fd, "", 1);
  if (result != 1) {
    close(fd);
    perror("Error writing last byte of the file");
    exit(EXIT_FAILURE);
  }
    
  map = mmap(0, FILESIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (map == MAP_FAILED) {
    close(fd);
    perror("Error mmapping the file");
    exit(EXIT_FAILURE);
  }
  map[6]=0;
  map[7]=0;
  
  //pour s'assurer que le fichier soit bien fermé en sortie
  if (atexit (fermeture_du_fichier) != 0)
    {
      perror("cannot set the exit function : shutting process");
      close(fd);
      exit(EXIT_FAILURE);
    }

  struct sigaction sa_alarm;
  set_quanta(quanta);
  // Initialisation des signaux
  sa_alarm.sa_handler = equilibre; // SIGALRM
  sa_alarm.sa_flags = 0;
  sigemptyset(&sa_alarm.sa_mask);
  sigaddset(&sa_alarm.sa_mask, SIGALRM);
  sigaddset(&sa_alarm.sa_mask, SIGALRM);
  sigaction(SIGALRM, &sa_alarm, NULL);
  printf("alarme mise en place\n");

  while(1)
    {
      sleep(1);
    }
    
  if (msync((void *) map, FILESIZE, MS_SYNC) == -1){
    perror("error in synchronizing the file before unmapping it");
  }

  //puis on remet le fichier à 0, du moins ses valeurs importantes
  map[0] = 0;
  map[3] = 0;
  map[6] = 0;
  map[7] = 0;
  if (munmap(map, FILESIZE) == -1) {
    perror("Error un-mmapping the file");
    /* Decide here whether to close(fd) and exit() or not. Depends... */
  }
    
  close(fd);
  return 0;
}
