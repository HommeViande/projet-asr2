SHELL := /bin/bash
TESTS=test-uthreads.ex test-hm.ex test-both.ex test-load_balancer.ex test-load_balancer1.ex
LIBS=libuthreads.so libhm.so
EXECS=load_balancer.ex
CFLAGS=-Wall -Wextra -g
LDLIBS=-pthread
LDTESTS=-L$(shell pwd) -luthreads -lhm
CC=gcc
CFILES=$(wildcard *.c)
HFILES=$(wildcard *.h)
OFILES=$(CFILES:.c=.o)

.PHONY:clean mrproper dist test

all: $(TESTS) $(EXECS)
libs: $(LIBS)

%.ex: %.o $(LIBS) 
	$(CC) $(LDLIBS) $(CFLAGS) $< -o $@ $(LDTESTS)

lib%.so: %.o
	$(CC) -shared $(LDLIBS) $(CFLAGS) $< -o $@

%.o: %.c
	$(CC) -fpic $(LDLIBS) $(CFLAGS) -c $< -o $@

clean:
	$(RM) $(OFILES) $(LIBS)

mrproper: clean
	$(RM) $(TESTS)

dist:
	tar -cv README.md $(CFILES) $(HFILES) Makefile -f rendu4.tar

test: $(TESTS:%.ex=%.test)

%.test: %.ex
	@echo "Testing $<"
	@LD_LIBRARY_PATH=$(shell pwd):$(shell echo $$LD_LIBRARY_PATH) ./$<
