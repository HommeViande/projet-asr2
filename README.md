# Projet d'ASR2

Librairie de threads utilisateur.

Par Michael Paper et Olivier Idir

---

Le fonctionnement des différentes fonctions est pensé pour être similaire à celles de la bibliothèque pthread.

```c
int init_vcpus(unsigned int number, int id, int numero);
```
initialise le scheduler pour un fonctionnement à `number` vCPUs en tant que vm numéro `id`

```c
void set_quanta(unsigned int quanta);
```
modifie l'intervalle entre deux appels du scheduleur, qui devient `quanta` milliseconde

```c
void set_vcpus(unsigned int number);
```
modifie le nombre de vCPUs utilisés par la librairie, pour qu'il
devienne `number`.

```c
int uthread_create(uthread_t *thread, void* (*start_routine) (void *), void *arg);
```
crée un uthread qui sera stocké dans `thread`, que l'on lance sur la fonction `start_routine` avec pour argument `arg`
  
```c
void uthread_kill(uthread_t thread, int sig);
```
envoie le signal `sig` au uthread `thread`

```c
void uthread_exit(void *retval);
```
**à mettre impérativement à la fin de chaque start_routine** : termine l'exécution d'un uthreads, et de renvoyer la valeur pointée par retval si ce thread a été joined à un moment

```c
void uthread_yield(void);
```
rend la main au scheduleur 

```c
void uthread_join(uthread_t thread, void **retval);
```
met le thread actif en attente jusqu'à ce que le uthread `thread` termine avec `uthread_exit` uet renvoie une valeur, qui sera stockée dans `*retval`

## À NOTER :

Les signaux SIGUSR1, SIGUSR2 et SIGALRM ne peuvent plus voir leur traitement modifié dans des handlers et leurs utilisations au sain du programme sont réservées pour le bon fonctionnement de la bibliothèque. Selon la même logique, `timer` ne peut pas non plus être utilisé, et donc `sleep` ne peut pas plus l'être

---

Pour la gestion du heap, là encore le fonctionnement des fonctions est similaire à celui de leurs homonymes.

```c
void init_heap();
```
normalement pas employé par l'utilisateur, définit le premier bloc mémoire et initialise le mutex des allocations

```c
void *malloc(size_t size);
```
alloue un bloc de taille `size` par la politique `first_fit`, si il n'en existe pas de tel appelle `sbrk()` pour pouvoir le créer. L'adresse renvoyée est celle où le user fera commencer ses données

```c
void free(void *ptr);
```
libère le bloc alloué par malloc ayant renvoyé `ptr`, et le fusionne avec ses éventuels voisins libres. Va ensuite éventuellement diminuer brk si c'est possible afin de ne pas occuper trop d'espace mémoire inutilement.

```c
void *realloc(void *ptr, size_t size);
```
ajuste la taille du bloc pointé pour qu'il soit de taille `size`. Si ce n'est pas possible (taille demandée trop grande) il alloue un autre bloc pour y stocker ses données et libère l'ancien. Si ptr = NULL, il fait simplement un malloc. Renvoie l'adresse "utilisable" du nouveau bloc.


---

Pour le protocole, l'approche choisie est assez proche d'un "handshake" :
On map le fichier partagé, et les deux processus ont un numéro associé lors de l'appel de `init_vcpus`. Les zones de fichier mappé s'agencent donc comme suit, les cases ayant la taille de ints
|case 0 : statut du processus 0 : éteint (0) ou actif(100) | case 1 : nombre de vcpus du processus 0 | case 2 : nombre de uthreads du processus 0 |
cases 3-5 : idem pour le processus 1 |
case 6 : case de communication entre le load_balancer et le processus 0 |
case 7 : idem pour le processus 1 |
espace restant : zone de recopie des contextes transférés

La case de communication peu prendre 4 valeurs :
0 -> rien à faire
1 -> le load_balancer dit au processus qu'il doit transférer un uthread
2 -> le processus signale au load_balancer qu'il a bien copié le uthread en mémoire
3 -> le load_balancer indique au processus qu'il y a en mémoire un uthread qu'il doit récupérer

Ainsi le protocole fait passer successivement les cases par les états :
LB : map[6] -> 1 | P_0 : map[6] = 2 (après recopie) | LB : map[6] = 0; map[7] = 3 | P_1 : map[7] = 0 (après recopie)

Cependant, même si le protocole semble marcher, le contexte recopié déclenche un segmentation fault dès qu'il est schédulé, et il semble en être de même parmi tous les groupes : le contexte doit ainsi contenir des adresses en dur (de tableau, de fonctions...) qui ne peuvent ainsi pas être transférées d'un processus à l'autre, car la pile d'exécution ne reconnaît alors pas ces objets aux adresses indiquées.
Il est aussi possible que ce soit une incompréhension de notre part à tous, mais étant donné l'opacité du type `context_t`, il est difficile de faire mieux ou de trouver comment corriger ce seg_fault si cela venait à être possible.

## Tests

Pour compiler, on appelle `make`. Nous avons implémenté trois tests, le premier sur les uthreads, le second sur le heap manager, et le troisième sur l'utilisation du heap manager par des uthreads. Pour les lancer, on appelle `make test-uthreads.test`, `make test-hm.test` ou `make test-both.test`. `make test` permet de les lancer de manière consécutive.

Pour ajouter un test, il suffit d'ajouter son nom dans la variable TESTS du Makefile.

